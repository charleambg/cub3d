/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsepath.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 21:55:40 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/09 09:49:03 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
** fonction recuper de path
** ret.i + 2 pour passer la lettre et potentiel espace
** ret.all++ pour comptabiliser la les donnes recuperer
*/

t_struct	ftnopath(t_struct ret)
{
	ret.i = ret.i + 2;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.no = ft_strdup(ret.buff + ret.i);
	printf("ret.no{%s}\n", ret.no);
	ret.i = 0;
	ret.all++;
	return (ret);
}

t_struct	ftsopath(t_struct ret)
{
	ret.i = ret.i + 2;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.so = ft_strdup(ret.buff + ret.i);
	printf("ret.so{%s}\n", ret.so);
	ret.i = 0;
	ret.all++;
	return (ret);
}

t_struct	fteapath(t_struct ret)
{
	ret.i = ret.i + 2;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.ea = ft_strdup(ret.buff + ret.i);
	printf("ret.ea{%s}\n", ret.ea);
	ret.i = 0;
	ret.all++;
	return (ret);
}

t_struct	ftwepath(t_struct ret)
{
	ret.i = ret.i + 2;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.we = ft_strdup(ret.buff + ret.i);
	printf("ret.we{%s}\n", ret.we);
	ret.i = 0;
	ret.all++;
	return (ret);
}

t_struct	ftspath(t_struct ret)
{
	ret.i = ret.i + 2;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.s = ft_strdup(ret.buff + ret.i);
	printf("ret.s{%s}\n", ret.s);
	ret.i = 0;
	ret.all++;
	return (ret);
}
