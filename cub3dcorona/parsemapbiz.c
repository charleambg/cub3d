/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsemapbiz.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/09 09:50:04 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/09 15:31:54 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_struct	remap(t_struct ret, int fd)
{
	int i;

	while (ret.buff[0] != '1')
	{
		i = 0;
		while (ret.buff[i] == ' ')
		{
			if (ret.buff[i + 1] == '1')
				return (ret);
			i++;
		}
		free(ret.buff);
		get_next_line(fd, &ret.buff);
	}
	return (ret);
}
/*
** check  longueur et largeur pour malloc
** ret.count  = ligne et ret.len = longueur
** boucle gnl puis close  pour relancer au meme endroit
*/

t_struct	checklen(t_struct ret, int fd, char *av)
{
	while (ret.buff[0] == '1' || ret.buff[0] == ' ')//&& ret.end == 1)
	{
		ret.count++;
		ret.i = 0;
		while (ret.buff[ret.i])
		{
			ret.i++;
			ret.len = (ret.i > ret.len) ? ret.i : ret.len;
		}
		free(ret.buff);
		ret.end = get_next_line(fd, &ret.buff);
	}
	if (ret.end != 0)
		ret.ok = 0;
	close(fd);
	fd = open(av, O_RDONLY);
	ret.end = get_next_line(fd, &ret.buff);
	//printf("buff{%s}\n" , ret.buff);
	ret = remap(ret, fd);
	printf("res{%d}{%d}\n", ret.count, ret.len);
	return (ret);
}

