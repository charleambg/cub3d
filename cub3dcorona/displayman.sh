#!/bin/sh
MAN_PATH='/usr/share/man/man3/mlx'
if [ $# == 0 ]
then
	echo "==> add an option to choose a man:"
	echo "-1) mlx.1"
	echo "-2) mlx_loop.1"
	echo "-3) mlx_new_image.1"
	echo "-4) mlx_new_window.1"
	echo "-5) mlx_pixel_put.1"
elif [ $1 == 1 ]; then
	man `echo "${MAN_PATH}.1"`
elif [ $1 == 2 ]; then
	man `echo "${MAN_PATH}_loop.1"`
elif [ $1 == 3 ]; then
	man `echo "${MAN_PATH}_new_image.1"`
elif [ $1 == 4 ]; then
	man `echo "${MAN_PATH}_new_window.1"`
elif [ $1 == 5 ]; then
	man `echo "${MAN_PATH}_pixel_put.1"`
else
	print "try again"
fi
