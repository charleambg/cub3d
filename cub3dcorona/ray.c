/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/13 17:02:03 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/12 20:11:53 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_put_pixel(t_all *all, int x, int y, int color)
{

	int (*pix_array)[all->ret->longueur];
	all->img->addr = mlx_get_data_addr(all->img->img, &all->img->bpp, &all->img->line_length, &all->img->endian);
	pix_array = (void *)all->img->addr; // cast for change 1 dimension array to 2 dimensions
    pix_array[y][x] = color; // set the pixel at the coord x,y with the color value
}

double	ftabs(double ray)
{
	ray = ( ray < 0) ? -ray : ray;
	return (ray);
}

void	line(int x, t_dda dda, t_all *all)
{
	int y;

	dda.drawend = (0 > dda.drawend) ? all->ret->hauteur : dda.drawend;
	y = 0;
	if ( x < all->ret->len * all->img->tilex)
		y = all->ret->count * all->img->tiley;
	while (y < dda.drawstart)
	{
		ft_put_pixel(all, x, y, all->img->crgb);
		y++;
	}
	while (y < dda.drawend)
	{
		ft_put_pixel(all, x ,y, all->vars->c);
		y++;
	}
	while (y < all->ret->hauteur)
	{
		ft_put_pixel(all, x ,y, all->img->frgb);
		y++;
	}
/*	if (x == 0 || x == 100 || x == 200 || x == 250|| x == 300 || x == 400 || x == 499)
	{
		while ( y > 0)
		{
			ft_put_pixel(all, x ,y, rouge);
			y--;
		}
	}
*/}
void	ddacalcul(t_all *all, t_dda *dda)
{
	int boom;

	boom = 0;
	while (boom == 0)
	{
		if (dda->sideDistX < dda->sideDistY)
		{
			dda->sideDistX = dda->sideDistX + dda->deltaDistX;
			all->vars->rx = all->vars->rx + dda->stepx;
			dda->side = 0;
		}
		else
		{
			dda->sideDistY = dda->sideDistY + dda->deltaDistY;
			all->vars->ry = all->vars->ry + dda->stepy;
			dda->side = 1;
		}
		if (all->ret->map[(int)all->vars->ry][(int)all->vars->rx] > 48)
			boom = 1;

	}
}

void	ray(t_all *all)
{
	t_dda	dda;
	int		x;
	double	w = all->ret->longueur;
	int		h = all->ret->hauteur;
	
	dda = initdda(dda);
	x = 0;
//	all->vars->x = all->vars->x + 0.5;
//	all->vars->y = all->vars->y + 0.5;
	while (x < w)
	{
		dda = initdda(dda);
		all->vars->ry = (int)(all->vars->y);
		all->vars->rx = (int)(all->vars->x);
		dda.cameraX = 2 * x / w - 1; //x-coordinate in dda.camera space
		dda.rayDirX = all->vars->vx + all->vars->planeX * dda.cameraX;
		dda.rayDirY = all->vars->vy + all->vars->planeY * dda.cameraX;
		dda.deltaDistX = sqrt(1 + (dda.rayDirY * dda.rayDirY) / (dda.rayDirX * dda.rayDirX));
		dda.deltaDistY = sqrt(1 + (dda.rayDirX * dda.rayDirX) / (dda.rayDirY * dda.rayDirY));
//		dda.deltaDistX = fabs(1 / dda.rayDirX);
//		dda.deltaDistY = fabs(1 / dda.rayDirY);
		if (dda.rayDirX < 0)
		{
			dda.stepx = -1;
			dda.sideDistX = (all->vars->x - all->vars->rx) * dda.deltaDistX;
		}
		else
		{
			dda.stepx = 1;
			dda.sideDistX = (all->vars->rx  + 1.0 - all->vars->x) * dda.deltaDistX;
		}
		if (dda.rayDirY < 0)
		{
			dda.stepy = -1;
			dda.sideDistY = (all->vars->y - all->vars->ry) * dda.deltaDistY;
		}
		else
		{
			dda.stepy = 1;
			dda.sideDistY = (all->vars->ry + 1.0 - all->vars->y) * dda.deltaDistY;
		}
	//	if ((int)(all->vars->ry * 2) % 2 == 1)
	//		all->vars->ry = all->vars->ry + 0.5;
	//	if ((int)(all->vars->rx * 2) % 2 == 1)
	//		all->vars->rx = all->vars->rx + 0.5;
		ddacalcul(all, &dda);
		if (dda.side == 0)
			dda.perpwalldist = (all->vars->rx - all->vars->x + (1 - dda.stepx) / 2)
				/ dda.rayDirX;
		else
			dda.perpwalldist = (all->vars->ry - all->vars->y + (1 - dda.stepy) / 2)
				/ dda.rayDirY;
	//	dda.perpwalldist = ( dda.perpwalldist < 1) ? 1 : dda.perpwalldist;
	
		droite(all, &dda , x);
		dda.lineh = (all->ret->hauteur / dda.perpwalldist);
		dda.drawstart = -dda.lineh / 2 + h / 2;
		dda.drawend = dda.lineh / 2 + h / 2;
		if (dda.drawstart < 0)
			dda.drawstart = 0;
		if (dda.drawend >= h)
			dda.drawend = h - 1;
		all->vars->c = (all->ret->map[(int)all->vars->ry][(int)all->vars->rx] == '1') ? foret : all->vars->c;
		all->vars->c = (all->ret->map[(int)all->vars->ry][(int)all->vars->rx] == '2') ? rouge : all->vars->c;
		if (dda.side == 1)
			all->vars->c = all->vars->c / 2;
		line(x, dda, all);
				if (x == 0 || x == 100 || x == 200 || x == 250|| x == 300 || x == 400 || x == 499)
				{
				printf("dda.perpwalldist'%lf' dda.lineh'%lf'\n" , dda.perpwalldist, dda.lineh);
				printf("dda.sideDistX'%lf' / positionx'%lf' / varsrx'%lf' / vectx '%lf'/ dda.deltaDistx'%lf' / dda.rayDirX'%lf' / dda.cameraX'%lf' / planeX'%lf'\n", dda.sideDistX, all->vars->x, all->vars->rx, all->vars->vx, dda.deltaDistX , dda.rayDirX, dda.cameraX , all->vars->planeX);
				printf("dda.sideDistY'%lf' / positony'%lf' / varsry'%lf' / vecty '%lf' / dda.deltaDisty'%lf' / dda.rayDirY'%lf' / planey'%lf'\n", dda.sideDistY, all->vars->y, all->vars->ry, all->vars->vy, dda.deltaDistY , dda.rayDirY, all->vars->planeY);
				printf("le x {%d}ledda.drawend{%d}ledrastart{%d} side = [%d]\n" , x , dda.drawend, dda.drawstart, dda.side);
		//		printf("mur[%d][%d]='%c'\n" ,all->vars->ry + all->vars->y , all->vars->rx + all->vars->x, all->ret->map[all->vars->ry + all->vars->y][all->vars->rx + all->vars->x]);
		printf("boom - mur[%d][%d]='%c'\n" ,(int)all->vars->ry, (int)all->vars->rx, all->ret->map[(int)all->vars->ry][(int)all->vars->rx]);
	//	printf("color = [%d]" , all->vars->c);
		printf("\n\n");
	//	printf(" int = [%d] / float = [%lf] \n" , (int)all->vars->ry, (int)all->vars->ry);
		}
				x++;
	}
//	all->vars->y = all->vars->y - 0.5;
//	all->vars->x = all->vars->x - 0.5;
		printf("\n\n");
		printf("\n\n");
		printf("\n\n");
		printf("\n\n");
}
