/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 13:32:37 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/12 20:11:52 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
int		inmap(t_all *all, int i)
{
	int ok;
	int x;
	int y1;
	
	ok = 0;
	x = all->vars->x * all->img->tilex + all->img->tilex / 2;
	y1 = all->vars->y * all->img->tiley + all->img->tiley;
	if (((x + posx) - all->img->tilex / 2 +
	((all->vars->vx * cos(-0.66)) - all->vars->vy *sin(-0.66)) * i) > 0)
		ok++;
	if (((x + posx) - all->img->tilex / 2 +
	((all->vars->vx * cos(-0.66)) - all->vars->vy *sin(-0.66)) * i) <
	(all->img->tilex * all->ret->len))
		ok++;
	if (((y1 + posy - all->img->tiley) + (all->vars->vx * sin(-0.66) +
	all->vars->vy * cos(-0.66)) * i) < (all->img->tiley * all->ret->count))
		ok++;
	return (ok);
}

void	droite(t_all *all, t_dda *dda, int x)
{
	float y;
	float y1;
	float	i;
	float	dx;
	float dy;
	float x1;
	float	x2;
	float y2;
	int ok;

	ok = 0;
	i = 0;
	x = all->vars->x * all->img->tilex + all->img->tilex / 2;
	x2 = all->vars->rx * all->img->tilex;
	x1 = all->vars->x * all->img->tilex;
	y2 = all->vars->ry * all->img->tiley;
	y1 = all->vars->y * all->img->tiley + all->img->tiley;
//	dx = (x2 - x1);
//	dy = (y2 - y1);
//
//pour x de x1 à x2 faire
/*	if (x < x2)
		while (x < x2)
		{
			y = y1 + dy * (x - x1) / dx;
			y = (y < 0.0001) ? y *= -1 : y;
		//	if ( y < 0)
			//	y = y1 - y;
//	y = 1 * 20 + 0 * -50 / 10
//	y = all->vars->y * all->img->tiley + dy * (x - all->vars->x * all->img->tilex) / dx;
		   ft_put_pixel(all, (int)x, (int)y , rouge);
			x++;
		}
	else 
		{
			x2 = x2 + all->img->tilex;

		while (x >= x2)
		{
			y = y1 + dy * (x - x1) / dx;
			y = (y < 0.0001) ? y *= -1 : y;
		//	if ( y < 0)
		//	if ( y < 0)
		//		y = y1 - y;
//	y = 1* 20 + 0 * -50 / 10
//	y = all->vars->y * all->img->tiley + dy * (x - all->vars->x * all->img->tilex) / dx;
		   ft_put_pixel(all, x, y , rouge);
			x--;
		}
		}
*/
		
//fin pour
//	while ( i < 
	while (i < 15 && inmap(all, i) == 3)
	{
		ft_put_pixel(all, (x + posx) - all->img->tilex / 2 + 
		((all->vars->vx * cos(-0.66)) - all->vars->vy *sin(-0.66)) * i , (y1 + posy - all->img->tiley) + (all->vars->vx * sin(-0.66) + all->vars->vy * cos(-0.66)) * i, rouge);
		i++;
	}
	i = 0;
	while (i < 15 && inmap(all, i) == 3)
	{
		ft_put_pixel(all, (x + posx) - all->img->tilex / 2 + all->vars->vx * i,
		(y1 + posy - all->img->tiley) + all->vars->vy * i, rouge);
		i++;
	}
	i = 0;
	while (i < 15 && inmap(all, i) == 3)
	{
		ft_put_pixel(all, (x + posx) - all->img->tilex / 2 + 
		((all->vars->vx * cos(0.66)) - all->vars->vy *sin(0.66)) * i , 
		y1 + posy - all->img->tiley + ((all->vars->vx * sin(0.66) + 
		all->vars->vy * cos(0.66)) * i), rouge);
		i++;
	}
}

void	square(t_all *all)
{
	all->img->x = all->vars->x * all->img->tilex;
	all->img->xb = all->img->x + all->img->tilex / 2;
	while (all->img->x <= all->img->xb)
	{
		all->img->y = all->vars->y * all->img->tiley;
		all->img->yb = all->img->y + all->img->tiley / 2;
		while (all->img->y <= all->img->yb)
		{
			ft_put_pixel(all, all->img->x + posx, all->img->y + posy, all->vars->c);
			all->img->y++;
		}
		all->img->x++;
	}
}

void	varsminiplus(t_all * all, int m, int i)
{
	if (all->ret->map[m][i] == 'W')
	{
		all->vars->vx = -1;
		all->vars->vy = 0;
		all->vars->planeX = 0;
		all->vars->planeY = -0.66;
	}

	if (all->ret->map[m][i] == 'S')
	{
		all->vars->vx = 0;
		all->vars->vy = 1;
		all->vars->planeX = -0.66;
		all->vars->planeY = 0;
	}
}


void	varsmini(t_all *all , int m, int i)
{
	all->vars->x = (all->vars->x == 0) ? i : all->vars->x;
	all->vars->y = (all->vars->y == 0) ? m : all->vars->y;
	if (all->ret->map[m][i] == 'E')
	{
		all->vars->vx = 1;
		all->vars->vy = 0;
		all->vars->planeX = 0;
		all->vars->planeY = 0.66;
	}
	if (all->ret->map[m][i] == 'N')
	{
		all->vars->vx = 0;
		all->vars->vy = -1;
		all->vars->planeX = 0.66;
		all->vars->planeY = 0;
	}
	varsminiplus(all, m ,i);
}

void	minicondi(t_all *all, int m, int i)
{
	while (all->img->y <= all->img->yb)
	{
		if (all->ret->map[m][i] == '1')
			ft_put_pixel(all, all->img->x + posx, all->img->y + posy,
			0x0000000);
		if (all->ret->map[m][i] == '0')
			ft_put_pixel(all, all->img->x + posx, all->img->y + posy, 0x00FFFFFF);

		if (all->ret->map[m][i] == '2')
			ft_put_pixel(all, all->img->x + posx, all->img->y + posy,
			0x000000FF);
		if (all->ret->map[m][i] == 'N' || all->ret->map[m][i] == 'S' ||
				all->ret->map[m][i] == 'W' || all->ret->map[m][i] == 'E')
			varsmini(all, m, i);
		all->img->y++;
	}
	all->ret->map[m][i] = (all->ret->map[m][i] == 'N' ||
			all->ret->map[m][i] == 'S' || all->ret->map[m][i] == 'W' ||
			all->ret->map[m][i] == 'E') ? '0' : all->ret->map[m][i];
}

void	minimap(t_all *all)
{
	int		i;
	int		m;

	m = 0;
	all->img->tiley = (int)((all->ret->hauteur / all->ret->count) / 5);
	all->img->tilex = (int)((all->ret->longueur / all->ret->len) / 5);
	while (all->ret->map[m] && m < all->ret->count)
	{
		i = 0;
		while (all->ret->map[m][i])
		{
			all->img->x = i * all->img->tilex;
			all->img->xb = all->img->x + all->img->tilex;
			while (all->img->x <= all->img->xb)
			{
				all->img->y = m * all->img->tiley;
				all->img->yb = all->img->y + all->img->tiley;
				minicondi(all, m, i);
				all->img->x++;
			}
			i++;
		}
		m++;
	}
}
