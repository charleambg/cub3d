/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initcub.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/13 13:19:13 by chgilber          #+#    #+#             */
/*   Updated: 2020/02/29 11:19:26 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <mlx.h>

t_data	colorgb(t_data img, t_struct ret)
{
	img.frgb = 65536 * ret.f[0] + 256 * ret.f[1] + ret.f[2];
	img.crgb = 65536 * ret.c[0] + 256 * ret.c[1] + ret.c[2];
	return (img);
}

t_struct	initret(t_struct ret, char **av)
{
	int		fd;
//	int		sizex;
//	int		sizey;
//	void	*mlx_ptr;
	
//	mlx_ptr = mlx_init();
//	sizex = 50;
//	sizey = 80;
	ret = initstrut(ret);
	fd = open(av[1], O_RDONLY);
	ret = parse(fd, av[1], ret);
	if (ret.ok == 0)
	{
		ret = freeret(ret);
		close(fd);
		fd = open(av[2], O_RDONLY);
		ret = parse(fd, av[2], ret);
	}
//	mlx_get_screen_size(mlx_ptr, sizex, sizey);
//	ret.hauteur = (ret.hauteur > sizey) ? sizey : ret.hauteur;
//	ret.longueur = (ret.longueur > sizex) ? sizex : ret.longueur;
	return (ret);
}

t_vars		initvars(t_vars vars, t_struct ret, char *av)
{
	vars.win = mlx_new_window(vars.mlx, ret.longueur, ret.hauteur, av);
	vars.x = 0;
	vars.y = 0;
	vars.vy = -1;
	vars.vx = 0;
	vars.planeX = 0;
	vars.planeY = 0.66;
	return (vars);
}

t_data		initimg(t_vars vars, t_struct ret, t_data img)
{
	img.addr = mlx_get_data_addr(img.img, &img.bpp, &img.line_length,
	&img.endian);
//	img.img = mlx_xpm_file_to_image(vars.mlx, ret.no, &ret.longueur,
//	&ret.hauteur);
	return (img);
}

t_dda	initdda(t_dda dda)
{
	dda.rayDirX = 0;
	dda.rayDirY = 0;
	dda.lineh = 0;
	dda.drawstart = 0;
	dda.drawend = 0;
	dda.deltaDistX = 0;
	dda.deltaDistY = 0;
	dda.sideDistX = 0;
	dda.sideDistY = 0;
	dda.perpwalldist = 0;
	dda.stepx = 0;
	dda.stepy = 0;
	dda.side = 0;
	return (dda);
}

