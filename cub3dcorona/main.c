/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/26 17:00:40 by chgilber          #+#    #+#             */
/*   Updated: 2020/02/29 21:27:27 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <mlx.h>

int		ft_exit(t_vars *vars)
{
	mlx_destroy_window(vars->mlx, vars->win);
	exit(0);
}

int		looping(t_all *all)
{
//	square(all);
//	all->vars->c = rouge;
//	droite(all);
	
//	ray(all);
	return (1);
}
t_struct	twice(t_struct ret)
{
	int		m;
	int		n;
	int		i;
	int		y;
	char	*tmp;
	char	*bigtmp;
	
	n = 0;
	m = 1;
	while (ret.count * 2 > n)
	{
		i = 0;
		y = 1;
		tmp = ft_calloc(10, ret.len);
		tmp = ft_strcat(tmp, ret.map[m], ret.len*2);
		while(ret.len *  2 > y)
		{
			ret.map[m][y] = tmp[i];
			y++;// = y + 2;
			if( y % 2 == 0)
				i++;
//			printf("ret.map[%d][%d]{%s} / tmp[%d]{%s}\n", m, y, ret.map[m], i , tmp);
		}
		tmp = ret.map[m];
		bigtmp = ft_calloc(10, ret.count);
		bigtmp = ret.map[m];
		ret.map[m + 1] = tmp;
	//	printf(" tmp = [%s] / bigtmp = [%s] / ret.map[%d] = [%s]\n" , tmp, bigtmp, m, ret.map[m]);
	m++;
	
	}
	m = 0;
	while( m < 20)
	{
		printf("ret.map[%d] = [%s]\n" , m, ret.map[m]);
		m++;
	}
	return (ret);
}

int		main(int ac, char **av)
{
	t_struct	ret;
	t_data		img;
	t_vars		vars;
	t_all		all;

	ret.i = ac;
	ret = initret(ret, av);
	vars.mlx = mlx_init();
	vars = initvars(vars, ret, *av);
	img.img = mlx_new_image(vars.mlx, ret.longueur, ret.hauteur);
	img = initimg(vars, ret, img);
	img = colorgb(img, ret);
	all.img = &img;
	all.ret = &ret;
	all.vars = &vars;
	//ret = twice(ret);
//	mlx_put_image_to_window(vars.mlx, vars.win, img.img, 0, 0);
	minimap(&all);
	ray(&all);
	mlx_put_image_to_window(vars.mlx, vars.win, img.img, 0, 0);	
	mlx_hook(vars.win, 17, 0, (*ft_exit), &vars);
	mlx_hook(vars.win, 2, 0, (*ft_keypress), &all);
	mlx_loop_hook(vars.mlx, (*looping), &all);
	mlx_loop(vars.mlx);
}
/*
 * position x et y en float, x = 10.5, y = 10.5
 * map[(int)position.x][(int)position.y] = map[10][10]
 * vecteur deplacement (x et y en float)
 * position.x += deplacement.x * speed;
 * position.y += deplacement.y * speed;
 */
