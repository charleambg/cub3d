/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keypress.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/11 18:28:29 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/12 20:11:51 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <mlx.h>



void	keyleft(t_all *all)
{
	double oldPlaneX = all->vars->planeX;
	double oldDirX = all->vars->vx;
	all->vars->dleft = 1;
	all->vars->vx = all->vars->vx * cos(0.1) - all->vars->vy * sin(0.1);
    all->vars->vy = oldDirX * sin(0.1) + all->vars->vy * cos(0.1);
    all->vars->planeX = all->vars->planeX * cos(0.1) - all->vars->planeY * sin(0.1);
    all->vars->planeY = oldPlaneX * sin(0.1) + all->vars->planeY * cos(0.1);
}

void	keyright(t_all *all)
{
	double oldPlaneX = all->vars->planeX;
	double oldDirX = all->vars->vx;
	all->vars->dright = 1;
	all->vars->vx = all->vars->vx * cos(-0.1) - all->vars->vy * sin(-0.1);
    all->vars->vy = oldDirX * sin(-0.1) + all->vars->vy * cos(-0.1);
    all->vars->planeX = all->vars->planeX * cos(-0.1) - all->vars->planeY * sin(-0.1);
    all->vars->planeY = oldPlaneX * sin(-0.1) + all->vars->planeY * cos(-0.1);
}

void	keymoov(t_all *all, int key)
{
	if (key == 1)
	{
		all->vars->y = all->vars->y + fast;
		all->vars->bottom = 1;
	}
	else if (key == 2)
	{
		all->vars->x = all->vars->x + fast;
		all->vars->right = 1;
	}
	else if (key == 0)
	{
		all->vars->x =all->vars->x - fast;
		all->vars->left = 1;
	}
	else if (key == 13)
	{
		all->vars->y = all->vars->y - fast;
		all->vars->top = 1;
	}
}

int		ftrotation(int keycode , t_all *all)
{
	if (keycode == 1 && all->vars->vx < -0.8)
		keycode = 2;
	else if (keycode == 13 && all->vars->vx < -0.8)
		keycode = 0;
	else if ( keycode == 2 && all->vars->vx < -0.8)
		keycode = 13;
	else if ( keycode == 0 && all->vars->vx < -0.8)
		keycode = 1;
	if (keycode == 1 && all->vars->vx > 0.8)
		keycode = 0;
	else if (keycode == 13 && all->vars->vx > 0.8)
		keycode = 2;
	else if ( keycode == 2 && all->vars->vx > 0.8)
		keycode = 1;
	else if ( keycode == 0 && all->vars->vx > 0.8)
		keycode = 13;
	if (keycode == 1 && all->vars->vy > 0.8)
		keycode = 13;
	else if (keycode == 13 && all->vars->vy > 0.8)
		keycode = 1;
	else if ( keycode == 2 && all->vars->vy > 0.8)
		keycode = 0;
	else if ( keycode == 0 && all->vars->vy > 0.8)
		keycode = 2;
	return (keycode);
}

int		ft_keypress(int keycode, t_all *all)
{
	keycode = ftrotation(keycode , all);
	if (keycode == 1 &&
	all->ret->map[(int)(all->vars->y + fast)][(int)(all->vars->x -0.1)] == '0'
	&& all->ret->map[(int)(all->vars->y + fast)][(int)(all->vars->x + 0.1)] == '0')
		keymoov(all, keycode);
	if (keycode == 2 &&
	all->ret->map[(int)(all->vars->y - 0.1)][(int)(all->vars->x + fast)] == '0'
	&& all->ret->map[(int)(all->vars->y + 0.1)][(int)(all->vars->x + fast)] == '0')
		keymoov(all, keycode);
	if (keycode == 0 
	&& all->ret->map[(int)(all->vars->y - 0.1)][(int)(all->vars->x - fast - 0.1)] == '0'
	&& all->ret->map[(int)(all->vars->y + 0.1)][(int)(all->vars->x - fast - 0.1)] == '0')
		keymoov(all, keycode);
	if (keycode == 13 && 
	all->ret->map[(int)(all->vars->y - fast - 0.1)][(int)(all->vars->x - 0.1)] == '0'
	&& all->ret->map[(int)(all->vars->y - fast - 0.1)][(int)(all->vars->x + 0.1)] == '0')
		keymoov(all, keycode);
	if (keycode == 123)
		keyright(all);
	if (keycode == 124)
		keyleft(all);
	if (keycode == 53)
		ft_exit(all->vars);
	minimap(all);
	ray(all);
//		droite(all, &dda , x);
	mlx_put_image_to_window(all->vars->mlx, all->vars->win, all->img->img, 0, 0);

	return (1);
}
