/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 19:33:40 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/12 20:11:54 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H

#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <mlx.h>
#include "libft/libft.h"
#include "GNL/get_next_line.h"
# define posx 0
# define posy 0
//# define foret 0x0000561B
# define foret 22043
//# define rouge 0x00ED0000
# define rouge 15532032
# define fast 0.25
typedef	struct	s_vars
{
	void			*mlx;
	void			*win;
	double			x;
	double			y;
	int					c;
	int					c0;
	int					left;
	int					dleft;
	int					dright;
	int					right;
	int					top;
	int					bottom;
	double				vx;
	double				vy;
	double				ry;
	double				rx;
	double				planeX;
	double				planeY;
}				t_vars;

typedef	struct	s_data
{
	void			*img;
	char			*addr;
	char			**imgcol;
	int				bpp;
	int				line_length;
	int				endian;
	float			x;
	float			xb;
	float			y;
	float			yb;
	float			tilex;
	float			tiley;
	int				crgb;
	int				ok;
	int				frgb;
}				t_data;

typedef struct	s_dda
{
	double	cameraX;
	double	rayDirX;
	double	rayDirY;
	double		lineh;
	int		drawstart;
	int		drawend;
	double	deltaDistX;
	double	deltaDistY;
	double	sideDistX;
	double	sideDistY;
	double	perpwalldist;
	double	stepx;
	double	stepy;
	int		side;
}			t_dda;

typedef	struct	s_struct
{
	int				i;
	int				end;
	int				count;
	int				len;
	int				comp;
	int				all;
	char			*buff;
	char			*no;
	char			*so;
	char			*we;
	char			*ea;
	char			*s;
	int				ok;
	int				m;
	int				max;
	int				f[3];
	int				hauteur;
	int				longueur;
	int				c[3];
	char			**map;
	char			**maptmp;
}				t_struct;

typedef	struct	s_vecteur
{
	int		x;
	int		y;
}				t_vecteur;

typedef	struct s_all
{
	t_struct		*ret;
	t_data			*img;
	t_vars			*vars;
}				t_all;

//t_struct g_ret;
t_data		colorgb(t_data img, t_struct ret);
t_struct	parse(int fd , char *av, t_struct ret);
t_struct	initret(t_struct ret , char **av);
t_vars		initvars(t_vars vars, t_struct ret , char *av);
t_data		initimg(t_vars vars, t_struct ret, t_data img);
t_struct	initstrut(t_struct ret);
t_dda		initdda(t_dda dda);
t_struct	ftreso(t_struct ret);
t_struct	ftfloor(t_struct ret);
t_struct	ftski(t_struct ret);
t_struct	ftmap(t_struct ret , int fd, char *av);
t_struct	check(t_struct ret);
t_struct	ftnopath(t_struct ret);
t_struct	ftsopath(t_struct ret);
t_struct	fteapath(t_struct ret);
t_struct	ftwepath(t_struct ret);
t_struct	ftspath(t_struct ret);
t_struct	freeret(t_struct ret);
t_struct	ftdata(t_struct ret, int fd, char *av);
t_struct	checklen(t_struct ret, int fd, char *av);

void	ft_put_pixel(t_all *all, int x, int y, int color);
int		ft_exit(t_vars *vars);
void	ray(t_all *all);
void	droite(t_all *all, t_dda *dda, int x);
int		ft_keypress(int keycode, t_all *all);
void	square(t_all *all);
void	dessin(t_data img);
void	minimap(t_all * all);
char		*ft_strcat(char *dest, const char *src, int end);
char		*ft_strcopy(char *dest, const char *src , int end);

//t_struct	fillmap(t_struct ret);
#endif
