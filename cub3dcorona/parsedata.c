/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsedata.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 21:52:47 by chgilber          #+#    #+#             */
/*   Updated: 2020/02/08 20:33:35 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
** fonction resolution
** skip space
** gestion d'erreur
*/

t_struct	ftreso(t_struct ret)
{
	ret.i++;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.longueur = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.longueur);
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.hauteur = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.hauteur);
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.ok = (ret.buff[ret.i] == '\0' || ret.hauteur == 0 || ret.longueur == 0)
	? 2 : 0;
//	printf("le-last[%c]", ret.buff[ret.i]);
	printf("\nhauteur %d\n", ret.hauteur);
	printf("longueur %d\n", ret.longueur);
	ret.i = 0;
	ret.all++;
	return (ret);
}

/*
** fonction sol
** skip ', '
** gestion d'erreur
*/

t_struct	ftfloor(t_struct ret)
{
	ret.i++;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.f[0] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.f[0]) + 1;
	ret.f[1] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.f[1]) + 1;
	ret.f[2] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.f[2]);
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.ok = (ret.buff[ret.i] == '\0') ? 2 : 0;
//	printf("le-last[%c]", ret.buff[ret.i]);
	printf("\nf0=%d, f1=%d, f2=%d\n", ret.f[0], ret.f[1], ret.f[2]);
	ret.i = 0;
	ret.all++;
	return (ret);
}

/*
** fonction plafond
** skip ', '
** gestion d'erreur
*/

t_struct	ftski(t_struct ret)
{
	ret.i++;
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.c[0] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.c[0]) + 1;
	ret.c[1] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.c[1]) + 1;
	ret.c[2] = ft_atoi(ret.buff + ret.i);
	ret.i = ret.i + ft_lennbr(ret.c[2]);
	while (ret.buff[ret.i] == ' ')
		ret.i++;
	ret.ok = (ret.buff[ret.i] == '\0') ? 2 : 0;
//	printf("le-last[%c]",  ret.buff[ret.i]);
	printf("\nc0=%d, c1=%d, c2=%d\n", ret.c[0], ret.c[1], ret.c[2]);
	ret.i = 0;
	ret.all++;
	return (ret);
}
