/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 21:42:44 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/09 13:02:49 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_struct	freeret(t_struct ret)
{
	int m;

	m = 0;
	ret = initstrut(ret);
	free(ret.no);
	free(ret.so);
	free(ret.ea);
	free(ret.we);
	free(ret.s);
	while (ret.map[m] && m < ret.count)
	{
		free(ret.map[m]);
		m++;
	}
	free(ret.map);
	return (ret);
}

t_struct	initstrut(t_struct ret)
{
	ret.end = 1;
	ret.m = 0;
	ret.all = 0;
	ret.i = 0;
	ret.count = 0;
	ret.len = 0;
	ret.ok = 2;
	return (ret);
}

t_struct	mapvalid(t_struct ret)
{
	int		m;
	int		i;
	int		verif;

	m = 0;
	verif = 0;
	while (ret.map[m] && m < ret.count)
	{
		i = 0;
		while (ret.map[m][i] && i < ret.len)
		{
			if (ret.map[m][i] == 'N' || ret.map[m][i] == 'S' ||
					ret.map[m][i] == 'E' || ret.map[m][i] == 'W')
				verif++;
			i++;
		}
		m++;
	}
	if (verif != 1)
		ret.ok = 0;
	return (ret);
}

t_struct	ftdata(t_struct ret, int fd, char *av)
{
	if (ret.buff[ret.i] == 'R' && ret.buff[ret.i + 1] == ' ')
		return (ftreso(ret));
	if (ret.buff[ret.i] == 'N' && ret.buff[ret.i + 1] == 'O' &&
	ret.buff[ret.i + 2] == ' ')
		return (ftnopath(ret));
	if (ret.buff[ret.i] == 'S' && ret.buff[ret.i + 1] == 'O' &&
	ret.buff[ret.i + 2] == ' ')
		return (ftsopath(ret));
	if (ret.buff[ret.i] == 'W' && ret.buff[ret.i + 1] == 'E' &&
	ret.buff[ret.i + 2] == ' ')
		return (ftwepath(ret));
	if (ret.buff[ret.i] == 'E' && ret.buff[ret.i + 1] == 'A' &&
	ret.buff[ret.i + 2] == ' ')
		return (fteapath(ret));
	if (ret.buff[ret.i] == 'S' && ret.buff[ret.i + 1] == ' ')
		return (ftspath(ret));
	if (ret.buff[ret.i] == 'F' && ret.buff[ret.i + 1] == ' ')
		return (ftfloor(ret));
	if (ret.buff[ret.i] == 'C' && ret.buff[ret.i + 1] == ' ')
		return (ftski(ret));
	if (/*ret.buff[ret.i] == '1' &&*/ ret.ok == 2 && ret.all == 8)
		return (ftmap(ret, fd, av));
	ret.ok = 0;
	return (ret);
}

t_struct	parse(int fd, char *av, t_struct ret)
{
	while (ret.end == 1)
	{
		ret.end = get_next_line(fd, &ret.buff);
		if (ret.buff[ret.i] && ret.end == 1)
		{
			while (ret.buff[ret.i] == ' ')
				ret.i++;
			ret = ftdata(ret, fd, av);
			free(ret.buff);
		}
	}
	ret = mapvalid(ret);
	if (ret.ok == 0)
	{
		write(1, "Error\nMap invalid\n", 19);
		return (ret);
	}
//	printf("le ok{%d}", ret.ok);
	return (ret);
}
