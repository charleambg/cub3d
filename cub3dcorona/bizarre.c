/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bizarre.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/11 18:23:32 by chgilber          #+#    #+#             */
/*   Updated: 2020/02/11 18:28:14 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <mlx.h>

void	my_mlx_pixel_put(t_data *data, int x, int y, int color)
{
	char		*dst;

	dst = data->addr + (y * data->line_length + x * (data->bpp / 8));
	*(unsigned int*)dst = color;
}

void	dessin(t_data img)
{
	int x;
	int y;
	int	test = 150;

	x = 500;
	y = 500;
	while( test > 0)
	{
		while ( x < 800 - test) //bleu
		{
			x++;
			y++;
			my_mlx_pixel_put(&img, x , y, 0x000000FF);
		}
		while ( x  > 500 ) //vert
		{
			x--;
			y++;
			my_mlx_pixel_put(&img, x , y, 0x0000FF00);
		}
		while( x  > 200 ) // rouge
		{
			x--;
			y--;
			my_mlx_pixel_put(&img, x , y, 0x00FF0000);
		}
		while( x < 500 - test) // jaune
		{
			x++;
			y--;
			my_mlx_pixel_put(&img, x , y, 0x00FFF000);
		}
		test = test - 10; // cadrillage
		y = y - 10;
	}
}
