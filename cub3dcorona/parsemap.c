/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsemap.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chgilber <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 21:56:46 by chgilber          #+#    #+#             */
/*   Updated: 2020/03/10 19:43:53 by chgilber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
** ret.comp comparer les lignes adjacentes
** ret.m lignes
** ret.ok = 0 = erreur
*/

t_struct	leftcondi(t_struct ret)
{
	int i;

	i = 0;
	while (i < ret.len )//&& ret.map[ret.m][i] == ' ')
	{
		if (ret.map[ret.m][i] == ' ')
		{
			if (ret.map[ret.m ][i - 1] == '0' || ret.map[ret.m][i + 1] == '0')
				ret.ok = 0;
			if (ret.m == 0)
				if (ret.map[ret.m + 1][i] == '0')
					ret.ok = 0;
			if (ret.m + 1 == ret.count)
				if (ret.map[ret.m - 1][i] == '0')
					ret.ok = 0;
			if (ret.m > 0 && ret.m + 1 < ret.count)
				if (ret.map[ret.m + 1][i] == '0' ||
						ret.map[ret.m - 1][i] == '0')
					ret.ok = 0;
			ret.map[ret.m][i] = '1';
		}
		i++;
	}
	return (ret);
}

t_struct	condi(t_struct ret)
{
	while (ret.len > ret.comp++)
	{
		if (ret.m == 0)
			if (ret.map[ret.m + 1][ret.comp - 1] == '0')
				ret.ok = 0;
		if (ret.m + 1 == ret.count)
			if (ret.map[ret.m - 1][ret.comp - 1] == '0')
				ret.ok = 0;
		if (ret.m > 0 && ret.m + 1 < ret.count)
			if (ret.map[ret.m + 1][ret.comp - 1] == '0' ||
			ret.map[ret.m - 1][ret.comp - 1] == '0')
				ret.ok = 0;
		ret.map[ret.m] = ft_strcat(ret.map[ret.m], "1", ret.len);
	}
	ret.comp = 0;
//	printf("ok%d]\n" , ret.ok);
	return (ret);
}

/*
** ret.m position de check , ret.max = 0 pour condi , ret.comp pour comparer
** ret.i ligne adjacente
** ret.map tab stock map
*/

t_struct	fillmap(t_struct ret)
{
	ret.m = 0;
	ret.max = 0;
	while (ret.m < ret.count)
	{
		ret.comp = ft_strlen(ret.map[ret.m]);// ligne base
		ret.i = ret.comp;
		if (ret.m + 1 < ret.count && ret.m > 0)
			ret.i = ft_strlen(ret.map[ret.m + 1]); // ligne comparer
			//	printf("m{%d}/comp{%d}/i{%d}/count{%d}/len{%d}\n",ret.m,ret.comp,ret.i,ret.count , ret.len);
		ret = condi(ret);
		ret = leftcondi(ret);
		printf("map{%s}(%d)\n", ret.map[ret.m], ret.m);
		ret.m++;
	}
	while (ret.map[0][ret.comp] == '1')
		ret.comp++;
//	if (ret.m + 1 == ret.count)
//	{
	while (ret.map[ret.m - 1][ret.max] == '1')
		ret.max++;
//	}
	ret.ok = (ret.comp == ret.len && ret.max == ret.len) ? ret.ok : 0;
	return (ret);
}

/*
** i et max curseur pour verifier la validiter
** check erreur
*/

t_struct	check(t_struct ret)
{
	ret.i = 0;
	ret.max = ft_strlen(ret.buff) - 1;
	while (ret.buff[ret.i])
	{
		if (ret.buff[ret.i] == '1' || ret.buff[ret.i] == ' ')
			ret.i++;
		else if (ret.buff[ret.i] == '2')
			ret.i++;
		else if (ret.buff[ret.i] == '0')
			ret.i++;
		else if (ret.buff[ret.i] == 'N')
			ret.i++;
		else if (ret.buff[ret.i] == 'S')
			ret.i++;
		else if (ret.buff[ret.i] == 'E')
			ret.i++;
		else if (ret.buff[ret.i] == 'W')
			ret.i++;
		else
			break ;
	}
	ret.i--;
//	printf("i{%d} et max{%d}\n" , ret.i, ret.max);
	ret.ok = (ret.i == ret.max) ? ret.ok : 0;
	return (ret);
}


/*
** checklen -> taille de map
** malloc
** ret.end return de gnl
** check  - >verification erreur composition tab
** et boucle gnl pour finir
** fillmap pour map rectangle
*/

t_struct	ftmap(t_struct ret, int fd, char *av)
{
	ret = checklen(ret, fd, av);
	if (ret.ok == 0 || !(ret.map = malloc(sizeof(char *) * ret.count * 2)))
	{
		ret.ok = 0;
		return (ret);
	}
	while (ret.end == 1 && ret.count > ret.m)
	{
		ret = check(ret);
		if (!(ret.map[ret.m] = malloc(sizeof(char *) * ret.len * 2)))
		{
			ret.ok = 0;
			return (ret);
		}
		ret.map[ret.m] = ft_strcopy(ret.map[ret.m], ret.buff, ret.len);
		ret.end = get_next_line(fd, &ret.buff);
//		printf("map{%s}(%d)\n", ret.map[ret.m], ret.m);
		ret.m++;
	}
	ret.m = 0;
	ret.end = 0;
	if (ret.ok == 0)
		return (ret);
//	printf("newmap\n");
	ret = fillmap(ret);
	return (ret);
}
