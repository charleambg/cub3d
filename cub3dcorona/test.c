// C program for DDA line generation 
#include "cub3d.h"
#include <stdio.h> 
#include <mlx.h> 
	while ( x < w)
{
  //calculate ray position and direction
	double cameraX = 2 * x / double(w) - 1; //x-coordinate in camera space
	double rayDirX = dirX + planeX * cameraX;
	double rayDirY = dirY + planeY * cameraX;
